.. _part11:

*************************************************************************************************
Partie 11 | Traits and monads
*************************************************************************************************

Questions by Group 24, Pierre Martou and Maxime Masy
=====================================================================

Question 1
""""""""""

The code below prints "meow" :

.. code-block:: scala

    abstract class Cat {
      def meow():String
    }
    class StrayCat extends Cat{
      override def meow(): String = "meow"
    }

    object main extends App{
      val cat= new StrayCat
      println(cat.meow())
    }


1. Now, we want the cat to be angry. Add code to this example, without modifying the classes already defined, so that
   "println(cat.meow)" prints "MEOW" instead of "meow". You can modify the main method, but the cat has to remain
   of the class StrayCat.

2. Identify the "base, core, stackable"  concepts in the code.

Code expected for the Question 1 :
""""""""""""""""""""""""""""""""""

.. code-block:: scala

  trait AngryCat extends Cat {
    override abstract def meow(): String = super.meow().toUpperCase()
  }

  object main extends App{
    val cat= new StrayCat with AngryCat
    println(cat.meow())
  }

Question 2
""""""""""
Complete this code to assign "sol" to the following list : List(22, 45, car). Ofcourse, you cannot hardcode it !

.. code-block:: scala

    val mygen = List(Some(22),Some(45),None,Some("car"))
    val sol = for ???

Answer to Question 2 :
""""""""""""""""""""""

.. code-block:: scala

  val mygen = List(Some(22),Some(45),None,Some("car"))
  val sol = for (Some(a) <- mygen) yield a
  println(sol)

Question 3
""""""""""

You are given two lists of integers, A and B. We want to create a new list, C, for every possible A and B lists. This new list contains :

* all the even numbers of A
* all elements of A that are in B

However, each time an element in B is found A, this element plus 3 is added to B. You are allowed to create functions to create C. You are also allowed to modify A and B in the process of creating C. The order of the elements in C doesn't matter, and it can have duplicates. Here's the skeleton of the code :

.. code-block:: scala

  var A = List(1, 2, 3, 4, 5, 7, 8, 10)
  var B = List(2,3,11)
  def main(args: Array[String]): Unit = {
    ????

    println(D)
  }

For example, this code should print List(2, 4, 8, 10, 3, 5) (or the same list with duplicates).

Hint 1 : List(1,2,3).filter(x => x == 2) returns List(2)

Possible answer to Q3 :
"""""""""""""""""""""""

.. code-block:: scala

  def isEven(x : Int): Boolean = {
    x%2 == 0
  }

  def process(x : Int): Boolean = {
    if(B.contains(x)) {
      B = B ++ List(x+3)
      true
    } else {
      false
    }
  }
  var A = List(1, 2, 3, 4, 5, 7, 8, 10)
  var B = List(2,3,11)
  def main(args: Array[String]): Unit = {
    val D1 = A.filter(x => isEven(x))

    val D2 = A.filter(x => process(x))

    val D = (D1 ++ D2).distinct

    println(D)
  }